package utils;

import org.apache.commons.lang3.RandomStringUtils;

public class StringUtils {
    private static String randomStr;

    public static String emailSubject(){
        return randomStr = RandomStringUtils.randomAlphabetic(10);
    }
    public static String emailMessage(){
        return randomStr = RandomStringUtils.randomAlphabetic(20);
    }
}
