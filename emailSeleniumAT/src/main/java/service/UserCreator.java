package service;

import model.User;
import org.apache.commons.lang3.StringUtils;

public class UserCreator {
    private static final String zohoUserEmail = TestDataReader.getTestData("test.zohoUser.email");
    private static final String zohoUserPassword = TestDataReader.getTestData("test.zohoUser.password");
    private static final String protonUserEmail = TestDataReader.getTestData("test.protonUser.email");
    private static final String protonUserPassword = TestDataReader.getTestData("test.protonUser.password");
    private static final String invalidPassword = "qwerty";

    public static User zohoUserWithCredentialsFromProperty() {
        return new User(zohoUserEmail, zohoUserPassword);
    }

    public static User protonUserWithCredentialsFromProperty() {
        return new User(protonUserEmail, protonUserPassword);
    }

    public static User protonUserWithEmptyPassword() {
        return new User(protonUserEmail, StringUtils.EMPTY);
    }

    public static User protonUserWithWrongPassword() {
        return new User(protonUserEmail, invalidPassword);
    }
    public static User protonUserWithEmptyPasswordTest() {
        return new User("testEpamATcourse@proton.me", "");
    }

    public static User protonUserWithWrongPasswordTest() {
        return new User("testEpamATcourse@proton.me", "qwerty");
    }
}
