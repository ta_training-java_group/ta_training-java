package pages.proton_mail_pages;

import model.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class ProtonSignInPage extends BasePage {
    @FindBy(xpath = "//*[@id='username']")
    private WebElement protonEmailUserField;

    @FindBy(xpath = "//*[@id='password']")
    private WebElement protonPasswordField;

    @FindBy(xpath = "//button[@type ='submit']")
    private WebElement signInButton;


    @FindBy(xpath = "//span[text()='Incorrect login credentials. Please try again']")
    private WebElement incorrectCredentialsPopup;

    @FindBy(xpath = "//span[text()='This field is required']")
    private WebElement thisFieldIsRequired;

    @FindBy(xpath = "//*[text()='Inbox']")
    private WebElement inBoxButton;


    public ProtonSignInPage(WebDriver driver) {

        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ProtonSignInPage protonUserEmailInput(User user) {
        waitElementIsVisible(protonEmailUserField, 15).sendKeys(user.getUserEmail());
        return this;
    }

    public ProtonSignInPage protonPasswordInput(User user) {
        protonPasswordField.sendKeys(user.getPassword());
        return this;
    }

    public ProtonInBoxPage protonSignInButtonClick() {
        signInButton.click();
        waitElementIsVisible(inBoxButton, 20);
        return new ProtonInBoxPage(driver);
    }

    public ProtonSignInPage protonSignInButtonClickFail() {
        signInButton.click();
        return this;
    }

    public boolean getIncorrectCredentialsOnLogin() {
        return waitElementIsVisible(incorrectCredentialsPopup, 15).isDisplayed();
    }

    public boolean getThisFieldIsRequiredMessage() {
        return waitElementIsVisible(thisFieldIsRequired, 15).isDisplayed();
    }

}
