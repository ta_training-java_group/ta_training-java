package pages.proton_mail_pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class ProtonHomePage extends BasePage {
    @FindBy(xpath = "//a[@target='_self']")
    private WebElement protonSingInButton;

    public ProtonHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public ProtonSignInPage protonHomeSignIn(){
        protonSingInButton.click();
        return new ProtonSignInPage(driver);
    }
}
