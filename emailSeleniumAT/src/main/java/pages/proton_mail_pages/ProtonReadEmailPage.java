package pages.proton_mail_pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class ProtonReadEmailPage extends BasePage {

    @FindBy(xpath = "//button[@data-testid='toolbar:movetotrash']")
            private WebElement trashButton;
    @FindBy(xpath = "//*[@id='proton-root']/div/div/div/div[1]")
            private WebElement emailMessage;
    @FindBy(xpath = "//iframe[@title='Email content']")
        private WebElement IframeEmailContent;
    public ProtonReadEmailPage(WebDriver driver) {

        super(driver);
        PageFactory.initElements(driver,this);
    }
    public String emailMessage(){

        waitElementIsVisible(IframeEmailContent,15);
        driver.switchTo().frame(IframeEmailContent);
        String emailContent = emailMessage.getText();
        driver.switchTo().defaultContent();
        return emailContent;
    }
    public ProtonInBoxPage deleteMail(){
        trashButton.click();
        return new ProtonInBoxPage(driver);
    }
}
