package pages.proton_mail_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

import java.util.function.Function;

public class ProtonInBoxPage extends BasePage {
    @FindBy(xpath = "//*[text()='Inbox']")
    private WebElement inBoxButton;
    @FindBy(xpath = "//span[text()='Unread email']")
    private WebElement unreadMark;
    @FindBy(xpath = "//span[text()='Unread email']/../..//span[2]/span")
    private WebElement unreadEmailSender;


    public ProtonInBoxPage(WebDriver driver) {

        super(driver);
        PageFactory.initElements(driver, this);
    }

    public boolean emailSubjectConfirm(String emailSubject){
        By emailSubjectEl = By.xpath("//span[text()='" + emailSubject +"']");
        Function<WebDriver, Boolean> func = new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                inBoxButton.click();
                if (driver.findElement(emailSubjectEl).isDisplayed()) {
                    return true;
                }
                return false;
            }
        };
        fluentWait(180, 5).until(func);
        return driver.findElement(emailSubjectEl).isDisplayed();
    }

    public ProtonReadEmailPage openUnreadEmail() {
        unreadEmailSender.click();
        return new ProtonReadEmailPage(driver);
    }


    public boolean mailUnreadCheck() {
        return unreadMark.isDisplayed();
    }

    public String getMailSender() {
        return unreadEmailSender.getAttribute("title");
    }

    public boolean inBoxButtonIsVisible() {
        return waitElementIsVisible(inBoxButton, 15).isDisplayed();

    }
}
