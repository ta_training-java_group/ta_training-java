package pages.zoho_mail_pages;

import model.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class ZohoLoginPasswordPage extends BasePage {
    @FindBy(xpath = "//*[@id='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//*[@id='nextbtn']")
    private WebElement signInButton;

    public ZohoLoginPasswordPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ZohoLoginPasswordPage enterPassword(User user) {
        waitElementIsVisible(passwordField, 15).sendKeys(user.getPassword());
        return this;
    }

    public ZohoChooseServicePage clickSingInButton() {
        signInButton.click();
        return new ZohoChooseServicePage(driver);
    }
}
