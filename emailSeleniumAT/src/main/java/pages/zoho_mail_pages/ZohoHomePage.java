package pages.zoho_mail_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.base_page.BasePage;

public class ZohoHomePage extends BasePage {

    By signIn = By.xpath("/html/body/div[2]/div[2]/div/a[4]");

    public ZohoHomePage(WebDriver driver) {
        super(driver);
    }

    public ZohoHomePage signIn(){
        driver.findElement(signIn).click();
        return this;
    }

}
