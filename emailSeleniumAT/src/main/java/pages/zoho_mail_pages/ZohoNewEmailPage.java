package pages.zoho_mail_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class ZohoNewEmailPage extends BasePage {
    @FindBy(xpath = "//div[@class='zmCAddWra']/div[1]/div/input")
    private WebElement toField;
    @FindBy(xpath = "//input[@placeholder='Subject']")
    private WebElement subjectField;
    @FindBy(xpath = "//span[text()='Send']")
    private WebElement sendEmailButton;
    @FindBy(xpath = "//li[@data-tooltip='No Subject']/span")
    private WebElement newEmailTab;
    By emailText = By.xpath("//body[@class=\"ze_body\"]/div/br");


    public ZohoNewEmailPage(WebDriver driver) {

        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ZohoNewEmailPage clickNewEmailTab() {
        waitElementIsVisible(newEmailTab, 30).click();
        return this;

    }

    public ZohoNewEmailPage writeEmailToSubject(String to, String subject) {
        waitElementIsVisible(toField, 10).sendKeys(to);
        subjectField.sendKeys(subject);
        return this;

    }

    public ZohoNewEmailPage writeEmailMessage(String message) {
        driver.switchTo().frame(0);
        driver.findElement(emailText).sendKeys(message);
        driver.switchTo().defaultContent();
        return this;
    }

    public ZohoMailInboxPage sendEmail() {
        sendEmailButton.click();
        waitUrlIs("https://mail.zoho.com/zm/#mail/folder/inbox", 5);
        return new ZohoMailInboxPage(driver);
    }
}
