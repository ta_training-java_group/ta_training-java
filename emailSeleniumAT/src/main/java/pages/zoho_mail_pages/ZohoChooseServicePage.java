package pages.zoho_mail_pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class ZohoChooseServicePage extends BasePage {
    @FindBy(xpath = "//div[text()='Mail']")
    private WebElement mailService;

    public ZohoChooseServicePage(WebDriver driver) {

        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ZohoMailInboxPage clickMailButton() {

        waitElementIsVisible(mailService, 10).click();
        waitUrlContains("https://mail.zoho.com/zm/#mail/folder/inbox", 30);
        return new ZohoMailInboxPage(driver);
    }
}
