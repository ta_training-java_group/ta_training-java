package pages.zoho_mail_pages;

import model.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class ZohoLogInEmailPhonePage extends BasePage {
    @FindBy(xpath = "//*[@id='login_id']")
    private WebElement EmailPhoneField;

    @FindBy(xpath = "//*[@id='nextbtn']")
    private WebElement nextButton;

    public ZohoLogInEmailPhonePage(WebDriver driver) {

        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ZohoLogInEmailPhonePage enterEmail(User user) {
        waitElementIsVisible(EmailPhoneField,5).sendKeys(user.getUserEmail());
        return this;
    }

    public ZohoLoginPasswordPage clickNextButton() {
        nextButton.click();
        return new ZohoLoginPasswordPage(driver);
    }
}
