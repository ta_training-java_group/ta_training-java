package pages.zoho_mail_pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class ZohoMailInboxPage extends BasePage {

    @FindBy(xpath = "//span[text()='Sign out']")
    private WebElement signOutButtom;

    @FindBy(xpath = "//img[@class='zmavatar__image']")
    private WebElement myProfile;
    @FindBy(xpath = "//*[text()='New Mail']")
    private WebElement newMailButton;

    public ZohoMailInboxPage(WebDriver driver) {

        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ZohoNewEmailPage clickNewEmailButton() {
        waitElementIsVisible(newMailButton, 15).click();
        return new ZohoNewEmailPage(driver);
    }

    public ZohoMailInboxPage enterMyProfile() {
        myProfile.click();
        return this;
    }

    public ZohoHomePage singOut() {
        waitElementIsVisible(signOutButtom, 5).click();
        waitUrlIs("https://www.zoho.com/es-xl/mail/", 15);
        return new ZohoHomePage(driver);
    }

}
