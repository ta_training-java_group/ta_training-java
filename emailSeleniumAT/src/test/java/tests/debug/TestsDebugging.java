package tests.debug;

import model.User;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.base.CommonAction;
import utils.StringUtils;

public class TestsDebugging extends CommonAction {

    @Test
    public void protonValidLoginPassword() {
        User protonUser = new User("testEpamATcourse@proton.me", "rive2MyHome");
        basePage
                .open(protonHomeUrl);
        protonHomePage
                .protonHomeSignIn();
        protonSignInPage
                .protonUserEmailInput(protonUser)
                .protonPasswordInput(protonUser)
                .protonSignInButtonClick();

    }

    @Test
    public void sendEmailFromZohoToProton() {
        User protonUser = new User("testEpamATcourse@proton.me", "drive2MyHome");
        User zohoUser = new User("testEpamATcourse1@zohomail.com", "drive2MyHome");
        String emailMessage = StringUtils.emailMessage();
        String emailSubject = StringUtils.emailSubject();
        zohoHomePage
                .open(zohoHomeUrl);
        zohoHomePage
                .signIn();
        zohoLogInEmailPhonePage
                .enterEmail(zohoUser)
                .clickNextButton();
        zohoLoginPasswordPage
                .enterPassword(zohoUser)
                .clickSingInButton();
        zohoChooseServicePage
                .clickMailButton();
        zohoMailInboxPage
                .clickNewEmailButton();
        zohoNewEmailPage
                .clickNewEmailTab()
                .writeEmailToSubject(protonUser.getUserEmail(), emailSubject)
                .writeEmailMessage(emailMessage)
                .sendEmail();
        zohoMailInboxPage
                .enterMyProfile()
                .singOut();
        protonHomePage
                .open(protonHomeUrl);
        protonHomePage
                .protonHomeSignIn();
        protonSignInPage
                .protonUserEmailInput(protonUser)
                .protonPasswordInput(protonUser)
                .protonSignInButtonClick();
        Assert.assertTrue(protonInBoxPage.emailSubjectConfirm(emailSubject));
        Assert.assertTrue(protonInBoxPage.getMailSender().equals(zohoUser.getUserEmail().toLowerCase()) && protonInBoxPage.mailUnreadCheck());
        protonInBoxPage.openUnreadEmail();
        Assert.assertEquals(protonReadEmailPage.emailMessage(), emailMessage);
        protonReadEmailPage.deleteMail();
    }
}
