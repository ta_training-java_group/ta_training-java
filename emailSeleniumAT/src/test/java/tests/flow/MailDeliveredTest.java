package tests.flow;

import model.User;
import org.testng.Assert;
import org.testng.annotations.Test;
import service.UserCreator;
import tests.base.CommonAction;
import utils.StringUtils;

public class MailDeliveredTest extends CommonAction {

    @Test
    public void protonOneUnreadMailCheck() {
        User protonUser = UserCreator.protonUserWithCredentialsFromProperty();
        User zohoUser = UserCreator.zohoUserWithCredentialsFromProperty();
        String emailMessage = StringUtils.emailMessage();
        String emailSubject = StringUtils.emailSubject();
        zohoHomePage
                .open(zohoHomeUrl);
        zohoHomePage
                .signIn();
        zohoLogInEmailPhonePage
                .enterEmail(zohoUser)
                .clickNextButton();
        zohoLoginPasswordPage
                .enterPassword(zohoUser)
                .clickSingInButton();
        zohoChooseServicePage
                .clickMailButton();
        zohoMailInboxPage
                .clickNewEmailButton();
        zohoNewEmailPage
                .clickNewEmailTab()
                .writeEmailToSubject(protonUser.getUserEmail(), emailSubject)
                .writeEmailMessage(emailMessage)
                .sendEmail();
        zohoMailInboxPage
                .enterMyProfile()
                .singOut();
        protonHomePage
                .open(protonHomeUrl);
        protonHomePage
                .protonHomeSignIn();
        protonSignInPage
                .protonUserEmailInput(protonUser)
                .protonPasswordInput(protonUser)
                .protonSignInButtonClick();
        Assert.assertTrue(protonInBoxPage.emailSubjectConfirm(emailSubject));
        Assert.assertTrue(protonInBoxPage.getMailSender().equals(zohoUser.getUserEmail().toLowerCase()) && protonInBoxPage.mailUnreadCheck());
        protonInBoxPage.openUnreadEmail();
        Assert.assertEquals(protonReadEmailPage.emailMessage(), emailMessage);
        protonReadEmailPage.deleteMail();
    }
}
