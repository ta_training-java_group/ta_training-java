package tests.smoke;

import model.User;
import org.testng.Assert;
import org.testng.annotations.Test;
import service.UserCreator;
import tests.base.CommonAction;

public class Smoke extends CommonAction {

    @Test
    public void protonValidLoginPassword() {
        User protonUser = UserCreator.protonUserWithCredentialsFromProperty();
        protonHomePage
                .open(protonHomeUrl);
        protonHomePage
                .protonHomeSignIn();
        protonSignInPage
                .protonUserEmailInput(protonUser)
                .protonPasswordInput(protonUser)
                .protonSignInButtonClick();
        Assert.assertTrue(protonInBoxPage.inBoxButtonIsVisible());
    }

    @Test
    public void emptyPassword() {
      User protonUser = UserCreator.protonUserWithEmptyPassword();
        protonHomePage
                .open(protonHomeUrl);
        protonHomePage
                .protonHomeSignIn();
        protonSignInPage
                .protonUserEmailInput(protonUser)
                .protonPasswordInput(protonUser)
                .protonSignInButtonClickFail();
        Assert.assertTrue(protonSignInPage.getThisFieldIsRequiredMessage());
    }

    @Test
    public void invalidPassword() {
        User protonUser = UserCreator.protonUserWithWrongPassword();
        protonHomePage
                .open(protonHomeUrl);
        protonHomePage
                .protonHomeSignIn();
        protonSignInPage
                .protonUserEmailInput(protonUser)
                .protonPasswordInput(protonUser)
                .protonSignInButtonClickFail();
        Assert.assertTrue(protonSignInPage.getIncorrectCredentialsOnLogin());
    }
}
