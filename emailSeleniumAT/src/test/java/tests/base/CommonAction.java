package tests.base;

import driver.DriverSingleton;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import pages.base_page.BasePage;
import pages.proton_mail_pages.ProtonHomePage;
import pages.proton_mail_pages.ProtonInBoxPage;
import pages.proton_mail_pages.ProtonReadEmailPage;
import pages.proton_mail_pages.ProtonSignInPage;
import pages.zoho_mail_pages.ZohoChooseServicePage;
import pages.zoho_mail_pages.ZohoLogInEmailPhonePage;
import pages.zoho_mail_pages.ZohoLoginPasswordPage;
import pages.zoho_mail_pages.ZohoMailInboxPage;
import pages.zoho_mail_pages.ZohoNewEmailPage;
import pages.zoho_mail_pages.ZohoHomePage;
import utils.TestListener;

@Listeners({TestListener.class})
public class CommonAction {
    protected WebDriver driver;
    protected BasePage basePage;
    protected ZohoHomePage zohoHomePage;
    protected ZohoLogInEmailPhonePage zohoLogInEmailPhonePage;
    protected ZohoLoginPasswordPage zohoLoginPasswordPage;
    protected ZohoChooseServicePage zohoChooseServicePage;
    protected ZohoMailInboxPage zohoMailInboxPage;
    protected ZohoNewEmailPage zohoNewEmailPage;
    protected ProtonHomePage protonHomePage;
    protected ProtonSignInPage protonSignInPage;
    protected ProtonInBoxPage protonInBoxPage;
    protected ProtonReadEmailPage protonReadEmailPage;
    protected final String zohoHomeUrl = "https://www.zoho.com/";
    protected final String protonHomeUrl = "https://proton.me/";

    @BeforeMethod()
    public void setUp(){
        driver = DriverSingleton.createDriver();
        basePage = new BasePage(driver);
        zohoHomePage = new ZohoHomePage(driver);
        zohoLogInEmailPhonePage = new ZohoLogInEmailPhonePage(driver);
        zohoLoginPasswordPage = new ZohoLoginPasswordPage(driver);
        zohoChooseServicePage = new ZohoChooseServicePage(driver);
        zohoMailInboxPage = new ZohoMailInboxPage(driver);
        zohoNewEmailPage = new ZohoNewEmailPage(driver);
        protonHomePage = new ProtonHomePage(driver);
        protonSignInPage = new ProtonSignInPage(driver);
        protonInBoxPage = new ProtonInBoxPage(driver);
        protonReadEmailPage = new ProtonReadEmailPage(driver);
    }

    @AfterMethod(alwaysRun = true)
    public void finishBrowser() {
        DriverSingleton.closeDriver();
        driver = null;
    }
}
